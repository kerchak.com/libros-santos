om tkinter import *
from tkinter import messagebox
import os
import mysql.connector
from mysql.connector import Error
import sys
py=sys.executable


#crear pantalla
class Lib(Tk):
    def __init__(self):
        super().__init__()
        self.a = StringVar()
        self.b = StringVar()
        self.maxsize(1200, 700)
        self.minsize(1200, 700)
        self.configure(bg="orange")
        self.title("SISTEMA DE GESTIÓN DE LIBROS")
        self.verificar()


#verificamos la entrada de datos
    def comprobar(self):
        if len(self.usuario_text.get()) == 0 or len(self.contra_text.get()) == 0:
            messagebox.showinfo("Error", "Usuario o contraseña no válidas")
        else:
            try:
                conexion = mysql.connector.connect(host='localhost',
                                         database='libros',
                                         user='root',
                                         password='')
                cursor = conexion.cursor()
                usuario = self.usuario_text.get()
                contra = self.contra_text.get()
                cursor.execute('Select * from `admin` where user= %s AND contra = %s ',(usuario,contra,))
                pc = cursor.fetchone()
                if pc:
                    self.destroy()
                    os.system('%s %s %s %s' % (py, 'opciones.py',usuario,contra))
                else:
                    print(pc)
                    messagebox.showinfo('Error', 'NO FUNCIONA, está mojao')
                    self.usuario_text.delete(0, END)
                    self.contra_text.delete(0, END)
            except Error:
                messagebox.showinfo('Error',"Algo ha ido mal, inténtalo de nuevo más tarde")

    def verificar(self):


        self.label = Label(self, text="Entrar", bg = 'orange' , fg = 'darkgreen', font=("Verdana", 24,'bold'))
        self.label.place(x=550, y=90)
        self.label1 = Label(self, text="Usuario" , bg = 'orange' , fg = 'darkgreen', font=("Verdana", 18, 'bold'))
        self.label1.place(x=370, y=180)
        self.usuario_text = Entry(self, textvariable=self.a, width=45)
        self.usuario_text.place(x=520, y=190)
        self.label2 = Label(self, text="Contraseña" , bg = 'orange' , fg = 'darkgreen', font=("Verdana", 18, 'bold'))
        self.label2.place(x=340, y=250)
        self.contra_text = Entry(self, show='*', textvariable=self.b, width=45)
        self.contra_text.place(x=520, y=255)
        self.butt = Button(self, text="Entrar",bg ='white', font=10, width=8, command=self.comprobar).place(x=580, y=300)
        self.label3 = Label(self, text="Libri Sacri", bg='orange', fg='white', font=("Comic Sans MS", 24, 'bold'))
        self.label3.place(x=350, y=30)

Lib().mainloop()







