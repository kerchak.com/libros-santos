from tkinter import *
from tkinter import messagebox
import re
from tkinter import ttk
import mysql.connector
from mysql.connector import Error
import os,sys
py=sys.executable

#creating window
class reg(Tk):
    def __init__(self):
        super().__init__()
        #self.iconbitmap('favicon.ico')
        self.maxsize(1200, 700)
        self.minsize(1200, 700)
        self.title('Añadir Usuario')
        self.canvas = Canvas(width=1200, height=700, bg='orange')
        self.canvas.pack()
#Creamos las variables que vamos a tratar
        u = StringVar()
        n = StringVar()
        p = StringVar()


        def insert():
            try:
                self.conn = mysql.connector.connect(host='localhost',
                                         database='libros',
                                         user='root',
                                         password='')
                self.myCursor = self.conn.cursor()
                self.myCursor.execute("Insert into admin(usuario,nombre,contra) values (%s,%s,%s)",[u.get(), n.get(), p.get()])
                self.conn.commit()
                messagebox.showinfo("OK", "Usuario introducido correctamente")
                ask = messagebox.askyesno("Confirmado", "¿Quieres añadir a otro usuario?")
                if ask:
                    self.destroy()
                    os.system('%s %s' % (py, 'usuarionuevo.py'))
                else:
                    self.destroy()
                    self.myCursor.close()
                    self.conn.close()
            except Error:
                messagebox.showinfo("Error", "Algo ha ido mal")
#Etiquetas y formulario
        Label(self, text='Detalles de usuario', bg='orange', fg='darkgreen', font=('Verdana', 25, 'bold')).place(x=130, y=22)
        Label(self, text='Usuario:', bg='orange', font=('Verdana', 10, 'bold')).place(x=70, y=82)
        Entry(self, textvariable=u, width=30).place(x=200, y=84)
        Label(self, text='Nombre:', bg='orange', font=('Verdana', 10, 'bold')).place(x=70, y=130)
        Entry(self, textvariable=n, width=30).place(x=200, y=132)
        Label(self, text='Contraseña:', bg='orange', font=('Verdana', 10, 'bold')).place(x=70, y=180)
        Entry(self, textvariable=p, width=30).place(x=200, y=182)
        Button(self, text="Enviar", width=15, command=insert).place(x=230, y=220)
reg().mainloop()
