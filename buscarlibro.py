from tkinter import *
from tkinter import ttk
from tkinter import messagebox
import mysql.connector
from mysql.connector import Error


class Search(Tk):
    def __init__(self):
        super().__init__()
        f = StringVar()
        g = StringVar()
        self.title("Buscar Libro")
        self.maxsize(1200,700)
        self.minsize(1200,700)
        self.canvas = Canvas(width=1200, height=700, bg='orange')
        self.canvas.pack()
        #self.iconbitmap('favicon.ico')
        l1=Label(self,text="Buscar Libros",bg='gray', font=("Verdana",20,'bold')).place(x=290,y=20)
        l = Label(self, text="Buscar por:",bg='gray', font=("Verdana", 12, 'bold')).place(x=60, y=96)
        def insert(data):
            self.listTree.delete(*self.listTree.get_children())
            for row in data:
                self.listTree.insert("", 'fin', text=row[0], values=(row[1], row[2], row[3]))
        def ge():
            if (len(g.get())) == 0:
                messagebox.showinfo('Error', 'Primero selecciona una opcióin')
            elif (len(f.get())) == 0:
                messagebox.showinfo('Error', 'Introduce el '+g.get())
            elif g.get() == 'Título del libro':
                try:
                    self.conn = mysql.connector.connect(host='localhost',
                                         database='libros',
                                         user='root',
                                         password='')
                    self.mycursor = self.conn.cursor()
                    self.mycursor.execute("Select * from catálogo where titulo LIKE %s",['%'+f.get()+'%'])
                    self.pc = self.mycursor.fetchall()
                    if self.pc:
                        insert(self.pc)
                    else:
                        messagebox.showinfo("Lo siento","O el título del libro está mal escrito o no lo tenemos, hermano")
                except Error:
                    messagebox.showerror("Error","Algo ha ido mal...")
            elif g.get() == 'Nombre del autor':
                try:
                    self.conn = mysql.connector.connect(host='localhost',
                                         database='libros',
                                         user='root',
                                         password='')
                    self.mycursor = self.conn.cursor()
                    self.mycursor.execute("Select * from catálogo where autor LIKE %s", ['%'+f.get()+'%'])
                    self.pc = self.mycursor.fetchall()
                    if self.pc:
                        insert(self.pc)
                    else:
                        messagebox.showinfo("Oop's","No hemos encontrado a este autor")
                except Error:
                    messagebox.showerror("Error","Algo ha ido mal...")
            elif g.get() == 'ISBN':
                try:
                    self.conn = mysql.connector.connect(host='localhost',
                                         database='libros',
                                         user='root',
                                         password='')
                    self.mycursor = self.conn.cursor()
                    self.mycursor.execute("Select * from catalogo where isbn LIKE %s", ['%'+f.get()+'%'])
                    self.pc = self.mycursor.fetchall()
                    if self.pc:
                        insert(self.pc)
                    else:
                        messagebox.showinfo("Lo sentimos","O ese ISBN es incorrecto o no lo tenemos")
                except Error:
                    messagebox.showerror("Error","Algo ha ido mal")
        b=Button(self,text="Buscar",width=15,bg='orange',font=("Verdana",10,'bold'),command=ge).place(x=460,y=148)
        c=ttk.Combobox(self,textvariable=g,values=["Título","Autor","ISBN"],width=40,state="readonly").place(x = 180, y = 100)
        en = Entry(self,textvariable=f,width=43).place(x=180,y=155)
        la = Label(self, text="Buscar",bg='orange', font=("Verdana", 15, 'bold')).place(x=100, y=150)

        def handle(event):
            if self.listTree.identify_region(event.x,event.y) == "separator":
                return "break"


        self.listTree = ttk.Treeview(self, height=13,columns=('Título', 'Autor', 'Disponibilidad'))
        self.vsb = ttk.Scrollbar(self,orient="vertical",command=self.listTree.yview)
        self.listTree.configure(yscrollcommand=self.vsb.set)
        self.listTree.heading("#0", text='ISBN', anchor='center')
        self.listTree.column("#0", width=120, anchor='center')
        self.listTree.heading("Título", text='Título')
        self.listTree.column("Título", width=200, anchor='center')
        self.listTree.heading("Autor", text='Autor')
        self.listTree.column("Autor", width=200, anchor='center')
        self.listTree.heading("Disponibilidad", text='Disponibilidad')
        self.listTree.column("Disponibilidad", width=200, anchor='center')
        self.listTree.bind('<Button-1>', handle)
        self.listTree.place(x=40, y=200)
        self.vsb.place(x=763,y=200,height=287)
        ttk.Style().configure("Treeview", font=('Times new Roman', 15))

Search().mainloop()
