from tkinter import *
from tkinter import messagebox
import mysql.connector
from mysql.connector import Error
#creating widow
class Rem(Tk):
    def __init__(self):
        super().__init__()
        #self.iconbitmap(favicon.ico')
        self.maxsize(400, 200)
        self.minsize(400, 200)
        self.title("Eliminar Administrador")
        self.canvas = Canvas(width=1200, height=700, bg='orange')
        self.canvas.pack()
        a = StringVar()
        def ent():
            if len(a.get()) ==0:
                messagebox.showinfo("Error","Introduce un DNI válido")
            else:
                d = messagebox.askyesno("Confirma", "¿Estás seguro de que el hermano administrador ha fallecido?")
                if d:
                    try:
                        self.conn = mysql.connector.connect(host='localhost',
                                         database='libros',
                                         user='root',
                                         password='')
                        self.myCursor = self.conn.cursor()
                        self.myCursor.execute("Delete from admin where id = %s",[a.get()])
                        self.conn.commit()
                        self.myCursor.close()
                        self.conn.close()
                        messagebox.showinfo("Confirmado","Usuario eliminado correctamente")
                        a.set("")
                    except:
                        messagebox.showerror("Error","Algo ha ido mal...")
        Label(self, text = "Introduce ID de usuario: ",bg='orange',fg='darkgreen',font=('Verdana', 12, 'bold')).place(x = 5,y = 40)
        Entry(self,textvariable = a,width = 37).place(x = 160,y = 44)
        Button(self, text='Eliminar', width=15, font=('arial', 10),command = ent).place(x=200, y = 90)



Rem().mainloop()
