from tkinter import *
from tkinter import messagebox
from tkinter import filedialog
import os
import sys
import mysql.connector
from mysql.connector import Error
py = sys.executable

#creating window
class Add(Tk):
    def __init__(self):
        super().__init__()
        #self.iconbitmap('favicon')
        self.maxsize(1200,700)
        self.minsize(1200,700)
        self.title('Añadir Hermano')
        self.canvas = Canvas(width=1200, height=700, bg='orange')
        self.canvas.pack()
        n = StringVar()
        p = StringVar()
        a = StringVar()
#verifying input
        def asi():
            if len(n.get()) < 1:
                messagebox.showinfo("Oop's", "Por favor, introduce tu nombre")
            elif len(p.get()) < 1:
                messagebox.showinfo("Oop's","Por favor, introduce tu teléfono")
            elif len(a.get()) < 1:
                messagebox.showinfo("Oop's", "Por favor, introduce tu dirección")
            else:
                try:
                    self.conn = mysql.connector.connect(host='localhost',
                                                        database='libros',
                                                        user='root',
                                                        password='')
                    self.myCursor = self.conn.cursor()
                    name1 = n.get()
                    pn1 = p.get()
                    add1 = a.get()
                    self.myCursor.execute("Insert into hermanos(nombre,telefono,direccion) values (%s,%s,%s)",[name1,pn1,add1])
                    self.conn.commit()
                    messagebox.showinfo("Hecho","Alta realizada correctamente")
                    ask = messagebox.askyesno("Confirmado","¿Quieres añadir a otro hermano?")
                    if ask:
                     self.destroy()
                     os.system('%s %s' % (py, 'usuarionuevo.py'))
                    else:
                     self.destroy()
                     self.myCursor.close()
                     self.conn.close()
                except Error:
                    messagebox.showerror("Error","Algo ha ido mal...")

        # label and input box
        Label(self, text='Detalles del hermano',bg='orange', fg='darkgreen', font=('Verdana', 25, 'bold')).pack()
        Label(self, text='Nombre:',bg='orange', font=('Verdana', 10, 'bold')).place(x=70, y=82)
        Entry(self, textvariable=n, width=30).place(x=200, y=84)
        Label(self, text='Teléfono:',bg='orange', font=('Verdana', 10, 'bold')).place(x=70, y=130)
        Entry(self, textvariable=p, width=30).place(x=200, y=132)
        Label(self, text='Dirección:',bg='orange', font=('Verdana', 10, 'bold')).place(x=70, y=180)
        Entry(self, textvariable=a, width=30).place(x=200, y=182)
        Button(self, text="Enviar",width = 15,command=asi).place(x=230, y=220)

Add().mainloop()
