from tkinter import *
from tkinter import messagebox
import os
from mysql.connector import Error
import mysql.connector
import sys
py=sys.executable

#crear una ventana
class Lib(Tk):
    def __init__(self):
        super().__init__()
        self.canvas = Canvas(width=1200, height=700, bg="orange")
        self.canvas.pack()
        self.maxsize(1200, 700)
        self.minsize(1200, 700)
        self.state('zoomed')
        self.title("SISTEMA DE GESTIÓN DE LIBROS")
        self.a = StringVar()
        self.b = StringVar()
        self.mimenu = Menu(self)
        #self.usuario=sys.argv[1]
        #self.contra=sys.argv[2]        
        def alibro():
            os.system('%s %s' % (py, 'libronuevo.py'))
        
        self.button = Button(self, text="Añadir Libro",
                              bg="white", font=("Verdana", 10, 'bold'),
                             width="10", command=alibro)
        self.button.place(x=580,y=300)

        def blibro():
            os.system('%s %s' % (py, 'borrarlibro.py'))
        
        self.button2 = Button(self, text="Borrar Libro",
                             bg="white", font=("Verdana", 10, 'bold'),
                             width="10", command=blibro)
        self.button2.place(x=580,y=400)
        
Lib().mainloop()       
