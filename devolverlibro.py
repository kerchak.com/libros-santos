from tkinter import *
from tkinter import messagebox
import os,sys
import mysql.connector
from mysql.connector import Error
from datetime import datetime,date
py = sys.executable


class ret(Tk):
    def __init__(self):
        super().__init__()
        #self.iconbitmap('favicon.ico')
        self.title("Devolver Libros")
        self.maxsize(1200,700)
        self.canvas = Canvas(width=500, height=417, bg='orange')
        self.canvas.pack()
        self.cal = 0
        a = StringVar()

        def qui():
            if len(a.get()) == '0':
                messagebox.showerror("Error","Por favor, introduce el ISBN del libro que vas a devolver")
            else:
                try:
                    self.conn = mysql.connector.connect(host='localhost',
                                                        database='libros',
                                                        user='root',
                                                        password='')
                    self.mycursor = self.conn.cursor()

                    self.mycursor.execute("Select isb from prestamos where fecha_devolucion = '' and isbn = %s",[a.get()])
                    temp = self.mycursor.fetchone()
                    if temp:
                        self.mycursor.execute("update libro set disponibilidad ='YES' where isbn = %s", [a.get()])
                        self.conn.commit()
                        now = datetime.now()
                        idate = now.strftime('%Y-%m-%d %H:%M:%S')
                        self.mycursor.execute("update prestamos set fecha_devolucion = %s where isbn = %s", [idate,a.get()])
                        self.conn.commit()
                        self.conn.close()
                        messagebox.showinfo('Info', 'Libro devuelto correctamente')
                        d = messagebox.askyesno("Confirme", "¿Quiere devolver más libros?")
                        if d:
                            self.destroy()
                            os.system('%s %s' % (py, 'devolverlibro.py'))
                        else:
                            self.destroy()
                    else:
                        messagebox.showinfo("Oop's", "Ese libro no estaba en préstamo")
                except Error:
                    messagebox.showerror("Error","Algo ha ido mal...")
        Label(self, text='Devolver Libros', fg='red',font=('Verdana', 25, 'bold')).pack()
        Label(self, text='Introduce ISBN', font=('Verdana', 12, 'bold')).place(x=20, y=120)
        Entry(self, textvariable=a, width=40).place(x=165, y=124)
        Button(self, text="Devolver", width=25, command=qui).place(x=180, y=180)
ret().mainloop()
