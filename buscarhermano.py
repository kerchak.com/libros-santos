from tkinter import *
from tkinter import ttk
from tkinter import messagebox
from PIL import ImageTk,Image
import os,glob
import mysql.connector
from mysql.connector import Error

class Search(Tk):
    def __init__(self):
        super().__init__()
        f = StringVar()
        g = StringVar()
        self.title("Buscar Hermano")
        self.maxsize(1200,700)
        self.canvas = Canvas(width=1366, height=768, bg='orange')
        self.canvas.pack()
        #self.iconbitmap('favicon.ico')
        l1=Label(self,text="Buscar Hermano",bg='orange', font=("Verdana",20,'bold')).place(x=290,y=40)
        l = Label(self, text="Buscar por:",bg='gray', font=("Verdana", 12, 'bold')).place(x=180, y=100)


        def insert(data):
            self.listTree.delete(*self.listTree.get_children())
            for row in data:
                self.listTree.insert("","fin",text = row[0], values = (row[1],row[2],row[3]))


        def ge():
            if (len(self.entry.get())) == 0:
                messagebox.showinfo('Error', 'Elige primer un item')
            elif (len(self.combo.get())) == 0:
                messagebox.showinfo('Error', 'Introduce el '+self.combo.get())
            elif self.combo.get() == 'Nombre':
                try:
                    self.conn = mysql.connector.connect(host='localhost',
                                         database='libros',
                                         user='root',
                                         password='')
                    self.mycursor = self.conn.cursor()
                    name = self.entry.get()
                    self.mycursor.execute("Select * from hermanos where nombre like %s",['%'+nombre+'%'])
                    pc = self.mycursor.fetchall()
                    if pc:
                        insert(pc)
                    else:
                        messagebox.showinfo("Oop's","Nombre no encontrado")
                except Error:
                    messagebox.showerror("Error", "Algo ha ido mal...")
            elif self.combo.get() == 'DNI':
                try:
                    self.conn = mysql.connector.connect(host='localhost',
                                         database='libros',
                                         user='root',
                                         password='')
                    self.mycursor = self.conn.cursor()
                    id = self.entry.get()
                    self.mycursor.execute("Select * from hermanos where dni like %s", ['%' + dni + '%'])
                    pc = self.mycursor.fetchall()
                    if pc:
                        insert(pc)
                    else:
                        messagebox.showinfo("Oop's", "No lo encuentro")
                except Error:
                    messagebox.showerror("Error", "Algo ha ido mal...")


        self.b= Button(self,text="Buscar",width=8,font=("Verdana",8,'bold'),command= ge )
        self.b.place(x=400,y=170)
        self.combo=ttk.Combobox(self,textvariable=g,values=["Nombre","DNI"],width=40,state="readonly")
        self.combo.place(x = 310, y = 105)
        self.entry = Entry(self,textvariable=f,width=43)
        self.entry.place(x=310,y=145)
        self.la = Label(self, text="Buscar",bg = 'gray', font=("Verdana", 15, 'bold')).place(x=180, y=140)

        def handle(event):
            if self.listTree.identify_region(event.x,event.y) == "separator":
                return "break"


        self.listTree = ttk.Treeview(self, height=13,columns=('Nombre', 'Teléfono', 'Dirección'))
        self.vsb = ttk.Scrollbar(self,orient="vertical",command=self.listTree.yview)
        self.listTree.configure(yscrollcommand=self.vsb.set)
        self.listTree.heading("#0", text='DNI', anchor='w')
        self.listTree.column("#0", width=100, anchor='w')
        self.listTree.heading("Nombre", text='Nombre')
        self.listTree.column("Nombre", width=200, anchor='center')
        self.listTree.heading("Teléfono", text='Teléfono')
        self.listTree.column("Teléfono", width=200, anchor='center')
        self.listTree.heading("Dirección", text='Dirección')
        self.listTree.column("Dirección", width=200, anchor='center')
        self.listTree.place(x=40, y=200)
        self.vsb.place(x=743,y=200,height=287)
        ttk.Style().configure("Treeview", font=('Times new Roman', 15))

Search().mainloop()
