from datetime import date, datetime
from tkinter import *
from tkinter import messagebox
import mysql.connector
from mysql.connector import Error
import os
import sys
py = sys.executable

#crear ventana
class issue(Tk):
    def __init__(self):
        super().__init__()
        #self.iconbitmap('favicon.ico')
        self.title('Administración')
        self.maxsize(1200, 700)

        self.canvas = Canvas(width=1200, height=700, bg='orange')
        self.canvas.pack()
        c = StringVar()
        d = StringVar()

#Verificar entrada
        def isb():
            if (len(c.get())) == 0 or len(d.get()) == 0:
                messagebox.showinfo('Error', 'Campos vacíos...')
            else:
             try:
                    self.conn = mysql.connector.connect(host='localhost',
                                                        database='libros',
                                                        user='root',
                                                        password='')
                    self.mycursor = self.conn.cursor()
                    self.mycursor.execute("Select disponibilidad from libro where disponibilidad = 'SI' and isbn = %s", [c.get()])
                    self.pc = self.mycursor.fetchall()
                    try:
                     if self.pc:
                        print("Todo Correcto")
                        book = c.get()
                        stud = d.get()
                        now = datetime.now()
                        idate = now.strftime('%Y-%m-%d %H:%M:%S')
                        self.mycursor.execute("Insert into prestamos(isbn,dni,fecha_prestamo,fecha_devolucion) values (%s,%s,%s,%s)",
                                              [book, stud, idate,''])
                        self.conn.commit()
                        self.mycursor.execute("Update libro set disponibilidad = 'NO' where book_id = %s", [book])
                        self.conn.commit()
                        messagebox.showinfo("OK", "Préstamo realizado correctamente")
                        ask = messagebox.askyesno("Confirmar", "¿Necesitas otro libro?")
                        if ask:
                            self.destroy()
                            os.system('%s %s' % (py, 'consulta.py'))
                        else:
                            self.destroy()
                     else:
                        messagebox.showinfo("Oop's", "ISBN "+c.get()+" no disponible")
                    except Error:
                        messagebox.showerror("Error", "Comprueba los detalles")
             except Error:
                    messagebox.showerror("Error", "Algo ha ido mal...")
                    
#label and input box
        Label(self, text='Préstamo de libros',bg = 'orange', font=('Verdana', 24)).place(x=135, y=40)
        Label(self, text='ISBN:',bg = 'orange', font=('Verdana', 15), fg='darkgreen').place(x=55, y=100)
        Entry(self, textvariable=c, width=40).place(x=160, y=106)
        Label(self, text='DNI:',bg = 'orange', font=('Verdana', 15), fg='darkgreen').place(x=20, y=150)
        Entry(self, textvariable=d, width=40).place(x=160, y=158)
        Button(self, text="Préstamo de libros", width=20, command=isb).place(x=200, y=200)
issue().mainloop()
