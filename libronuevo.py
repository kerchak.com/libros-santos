from tkinter import *
from tkinter import messagebox
import os
from mysql.connector import Error
import mysql.connector
import sys
py=sys.executable

class Lib(Tk):
    def __init__(self):
        super().__init__()
        self.canvas = Canvas(width=1200, height=700, bg="orange")
        self.canvas.pack()
        self.maxsize(1200, 700)
        self.minsize(1200, 700)
        self.state('zoomed')
        self.title("SISTEMA DE GESTIÓN DE LIBROS")
        self.a = StringVar()
        self.b = StringVar()
        self.c = StringVar()
        self.d = StringVar()
        self.e = StringVar()
        self.f = StringVar()
        self.g = StringVar()
        self.mimenu = Menu(self)

        #Verificamos la entrada de datos
        def añadir():
            if len(self.b.get()) == 0 or len(self.d.get()) == 0 or len(self.e.get()) == 0:
                messagebox.showerror("Error","Algunos campos son obligatorios")
            else:
                try:
                    self.conn = mysql.connector.connect(host="localhost",
                                                        database="libros",
                                                        user="root",
                                                        password = '')
                    self.myCursor = self.conn.cursor()
                    self.myCursor.execute("Insert into catalogo(titulo, autor, isbn, genero, categoria, editorial) values (%s,%s,%s,%s,%s,%s)",[self.b.get(),self.c.get(),self.d.get(),self.e.get(),self.f.get(),self.g.get()]) 
                    self.conn.commit()
                    messagebox.showinfo("Información","Libro añadido correctamente")
                    pregunta = messagebox.askyesno("Confirmar", "¿Quieres añadir otro libro?")
                    if pregunta:
                        self.destroy()
                        os.system('%s %s' % (py, 'libronuevo.py'))
                    else:
                        self.destroy()
                        os.system('%s %s' % (py, 'opciones.py'))
                except Error:
                    messagebox.showerror("Error","Comprueba lo que estás haciendo")
                
        # Creamos botones
        Label(self, text='').pack()
        Label(self, text='Libros Santos:',
              bg='orange',fg='darkgreen',
              font=('Verdana', 20, 'bold')).place(x=150, y=70)
        Label(self, text='').pack()
        Label(self, text='Título:',
              bg='orange',fg='darkgreen',
              font=('Verdana', 13, 'bold')).place(x=60, y=180)
        Entry(self, textvariable=self.b, width=30).place(x=170, y=182)

        Label(self, text='Autor:',
              bg='orange',fg='darkgreen',
              font=('Verdana', 13, 'bold')).place(x=60, y=220)
        Entry(self, textvariable=self.c, width=30).place(x=170, y=222)

        Label(self, text='ISBN:',
              bg='orange',fg='darkgreen',
              font=('Verdana', 13, 'bold')).place(x=60, y=260)
        Entry(self, textvariable=self.d, width=30).place(x=170, y=262)

        Label(self, text='Género:',
              bg='orange',fg='darkgreen',
              font=('Verdana', 13, 'bold')).place(x=60, y=300)
        Entry(self, textvariable=self.e, width=30).place(x=170, y=302)

        Label(self, text='Categoría:',
              bg='orange',fg='darkgreen',
              font=('Verdana', 13, 'bold')).place(x=60, y=340)
        Entry(self, textvariable=self.f, width=30).place(x=170, y=342)

        Label(self, text='Editorial:',
              bg='orange',fg='darkgreen',
              font=('Verdana', 13, 'bold')).place(x=60, y=380)
        Entry(self, textvariable=self.g, width=30).place(x=170, y=382)
        Button(self, text="Enviar", command=añadir).place(x=245, y=422)
Lib().mainloop()









